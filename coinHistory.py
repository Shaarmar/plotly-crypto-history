# -*- coding: utf-8 -*-
import csv
import json
import random
import re
import sys
import time
from datetime import date, datetime

import numpy as np
import pandas as pd
import plotly.figure_factory as FF
import plotly.graph_objs as go
import plotly.plotly as py
import requests
from bs4 import BeautifulSoup

end_date = str(date.today()).replace("-","")
base_url = "https://coinmarketcap.com/currencies/{0}/historical-data/?start=20130428&end="+end_date
currency_name_list  = ["bitcoin", "ethereum", "litecoin", "bitcoin-cash", "neo", "stratis", "dash", "ethereum-classic", "monero"]



def get_data(currency_name):
    url = base_url.format(currency_name)
    html_response = requests.get(url).text.encode('utf-8')
    soup = BeautifulSoup(html_response, 'html.parser')
    table = soup.find_all('table')[0]
    elements = table.find_all("tr")
    
    with open("./{0}_price.csv".format(currency_name.replace("-","_")),"w") as ofile:
        writer = csv.writer(ofile)
        for element in elements:
            writer.writerow( element.get_text().strip().split("\n") )
    time.sleep(1)

if __name__ == "__main__":
    for currency_name in currency_name_list:
        print('Currency : ' + currency_name)
        get_data(currency_name)
        pass


bitcoin = pd.read_csv('bitcoin_price.csv'
    ,usecols=["Date", "Close"])
bitcoin["Date"] = pd.to_datetime(bitcoin['Date'])
    
ether= pd.read_csv('ethereum_price.csv'
    ,usecols=["Date", "Close"])
ether["Date"] = pd.to_datetime(ether['Date'])

litecoin= pd.read_csv('litecoin_price.csv'
    ,usecols=["Date", "Close"])
litecoin["Date"] = pd.to_datetime(litecoin['Date'])

bitcoin_cash= pd.read_csv('bitcoin_cash_price.csv'
    ,usecols=["Date", "Close"])
bitcoin_cash["Date"] = pd.to_datetime(bitcoin_cash['Date'])

dash= pd.read_csv('dash_price.csv'
    ,usecols=["Date", "Close"])
dash["Date"] = pd.to_datetime(dash['Date'])
    
ether_classic= pd.read_csv('ethereum_classic_price.csv'
    ,usecols=["Date", "Close"])
ether_classic["Date"] = pd.to_datetime(ether_classic['Date'])

monero= pd.read_csv('monero_price.csv'
    ,usecols=["Date", "Close"])
monero["Date"] = pd.to_datetime(monero['Date'])

neo= pd.read_csv('neo_price.csv'
    ,usecols=["Date", "Close"])
neo["Date"] = pd.to_datetime(neo['Date'])

stratis= pd.read_csv('stratis_price.csv'
    ,usecols=["Date", "Close"])
stratis["Date"] = pd.to_datetime(stratis['Date'])



d = {'BTC' : pd.Series(bitcoin["Close"]),
      'ETH' : pd.Series(ether["Close"]),
      'LTC' : pd.Series(litecoin["Close"]),
      'NEO' : pd.Series(neo["Close"]),
      'STRAT' : pd.Series(stratis["Close"]),
      'BCH' : pd.Series(bitcoin_cash['Close']),
      'DASH' : pd.Series(dash['Close']),
      'ETC' : pd.Series(ether_classic['Close']),
      'XMR' : pd.Series(monero['Close']),
      
    }


df = pd.DataFrame(d)

bitcoinTrace = go.Scatter(
    x=bitcoin['Date'],
    y=df['BTC'],
    name='BTC'
)
etherTrace = go.Scatter(
    x=ether['Date'],
    y=df['ETH'],
    name='ETH',
)

litecoinTrace = go.Scatter(
    x=litecoin['Date'],
    y=df['LTC'],
    name='LTC',  
)

neoTrace = go.Scatter(
    x=neo['Date'],
    y=df['NEO'],
    name='NEO',   
)

stratisTrace = go.Scatter(
    x=stratis['Date'],
    y=df['STRAT'],
    name='STRAT', 
)


bitcoin_cashTrace = go.Scatter(
    x=bitcoin_cash['Date'],
    y=df['BCH'],
    name='BCH',
)

ethereum_classicTrace = go.Scatter(
    x=ether_classic['Date'],
    y=df['ETC'],
    name='ETC',  
)

moneroTrace = go.Scatter(
    x=monero['Date'],
    y=df['XMR'],
    name='XMR',  
)

dashTrace = go.Scatter(
    x=dash['Date'],
    y=df['DASH'],
    name='DASH',
)

data = [bitcoinTrace, etherTrace, litecoinTrace, ethereum_classicTrace, bitcoin_cashTrace, dashTrace, stratisTrace, moneroTrace, neoTrace]

layout = go.Layout(
    title='Cryptocurrency History',
    yaxis=dict(
        title='USD Price',
        showticklabels= not False,
            type='log',
        nticks = 6
    ),
    xaxis=dict(
        rangeselector=dict(
            buttons=list([
                dict(count=1,
                     label='1m',
                     step='month',
                     stepmode='backward'),
                dict(count=6,
                     label='6m',
                     step='month',
                     stepmode='backward'),
                dict(step='all')
            ])
        ),
        rangeslider=dict(),
        type='date'
    )
)

fig = go.Figure(data=data, layout=layout)

py.plot(fig, filename='pandas-time-series')
